#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <set>

namespace {
	std::vector<int> PrefixFunction(const std::string& str) {
		std::vector<int> prefix_func(str.size(), 0);
		for (size_t i = 1; i < str.size(); ++i) {
			int j = prefix_func[i - 1];
			while (j > 0 && str[i] != str[j])
				j = prefix_func[j - 1];
			if (str[i] == str[j])  ++j;
			prefix_func[i] = j;
		}
		return prefix_func;
	}

	size_t Overlap(const std::string& l, const std::string& r) {
		return PrefixFunction(r + "#" + l).back();
	}

	bool IsSubstr(const std::string& str, const std::string& text) {
		auto prefix_func = PrefixFunction(str + "#" + text);
		for (size_t j = 0; j < prefix_func.size(); ++j)
		{
			if (str.size() == prefix_func[j]) {
				return true;
			}
		}
		return false;
	}

	std::vector<std::vector<size_t>> BuildOverlapTable(const std::vector<std::string>& strings)	{
		size_t n = strings.size();
		std::vector<std::vector<size_t>> res(n, std::vector<size_t>(n));
		for (size_t i = 0; i < n; ++i) {
			for (size_t j = 0; j < n; ++j)
			{
				if (i == j) res[i][j] = 0;
				else res[i][j] = Overlap(strings[i], strings[j]);
			}
		}
		return res;
	}

	std::vector<std::string> RemoveSubstrings(const std::vector<std::string>& strings) {
		std::vector<std::string> res;
		for (size_t i = 0; i < strings.size(); ++i)
		{
			auto& s = strings[i];
			bool ok = true;
			for (size_t j = 0; j < i; ++j)
				ok &= (s != strings[j]);
			if (!ok) continue;
			for (auto& o : strings)
			{
				if (s == o) continue;
				ok &= !IsSubstr(s, o);
			}
			if (!ok) continue;
			res.push_back(s);
		}
		return res;
	}
}

struct ShortestCommonSuperstring {
public:
    explicit ShortestCommonSuperstring(const std::vector<std::string> strings) : strings(std::move(strings)) {}
	virtual std::string GetResult() = 0;
protected:
	std::vector<std::string> strings;
};

struct NPShortestSuperstring : public ShortestCommonSuperstring {
public:
	NPShortestSuperstring(const std::vector<std::string> strings) :
		ShortestCommonSuperstring(strings) {}

	std::string GetResult() override {
		std::vector <std::string> without_substrs = RemoveSubstrings(strings);
		std::vector<int> permutation(without_substrs.size());
		std::string res;
		size_t n = without_substrs.size();
		for (size_t i = 0; i < n; ++i)  {
			res += without_substrs[i];
			permutation[i] = i;
		}
		auto t = BuildOverlapTable(without_substrs);
		do
		{
			std::string now = without_substrs[permutation[0]];
			for (size_t i = 1; i < n; ++i) {
				auto overlap = t[permutation[i - 1]][permutation[i]];
				now += without_substrs[permutation[i]].substr(overlap);
			}
			if (res.size() > now.size()) res = std::move(now);
		} while (std::next_permutation(permutation.begin(), permutation.end()));
		return res;
	}
};


struct GreedyShortestSuperstring : public ShortestCommonSuperstring {
public:
	GreedyShortestSuperstring(const std::vector<std::string> strings) :
		ShortestCommonSuperstring(strings) {}

	std::string GetResult() override {
		std::vector<std::string> s = RemoveSubstrings(strings);
		size_t n = s.size();
		while (n > 1)
		{
			size_t maxoverlap = 0;
			size_t msi = 0;
			size_t msj = 1;
			for (size_t i = 0; i < n; ++i)
			{
				for (size_t j = 0; j < n; ++j)
				{
					if (i == j) continue;
					size_t curoverlap = Overlap(s[i], s[j]);
					if (curoverlap > maxoverlap)
					{
						maxoverlap = curoverlap;
						msi = i;
						msj = j;
					}
				}
			}
			s.push_back(s[msi] + s[msj].substr(maxoverlap));
			s.erase(s.begin() + std::max(msi, msj));
			s.erase(s.begin() + std::min(msi, msj));
			n -= 1;
		}
		return s.front();
	}
};

struct AssignShortestSuperstring : public ShortestCommonSuperstring {
	AssignShortestSuperstring(const std::vector<std::string> strings) :
		ShortestCommonSuperstring(strings) {}

	static std::vector<size_t> Assignment(const std::vector<std::vector<size_t>>& a) {
		size_t n = a.size();
		std::set<size_t> col_allow, row_allow;
		std::vector<size_t> res(n);
		for (size_t i = 0; i < n; ++i)
		{
			col_allow.insert(i);
			row_allow.insert(i);
		}
		for (size_t iter = 0; iter < n; ++iter)
		{
			size_t max = 0, maxi = 0, maxj = 0;

			for (auto &i : row_allow)
			{
				for (auto &j : col_allow)
				{
					if (a[i][j] > max || max == 0)
					{
						max = a[i][j];
						maxi = i;
						maxj = j;
					}
				}
			}
			res[maxi] = maxj;
			row_allow.erase(maxi);
			col_allow.erase(maxj);
		}
		return res;
	}

	std::string GetResult() override {
		std::vector<std::string> s = RemoveSubstrings(strings);
		auto overlaps = BuildOverlapTable(s);
		auto assign = Assignment(overlaps);
		std::string res;
		std::vector<bool> used(s.size(), false);
		for (size_t v = 0; v < s.size(); ++v)
		{
			if (used[v]) continue;
			std::vector<size_t> cycle;
			size_t go = v;
			while (!used[go])
			{
				used[go] = true;
				cycle.push_back(go);
				go = assign[go];
			}
			size_t min = -1;
			size_t off = -1;
			for (size_t i = 0; i < cycle.size(); ++i)
			{
				int j = (i + 1) % cycle.size();
				if (-1 == min || overlaps[cycle[i]][cycle[j]] < min)
				{
					min = overlaps[cycle[i]][cycle[j]];
					off = j;
				}
			}
			res += s[cycle[off]];

			for (size_t iter = 0; iter < cycle.size() - 1; ++iter)
			{
				int next = (off + 1) % cycle.size();
				int overlap = overlaps[cycle[off]][cycle[next]];
				res += s[cycle[next]].substr(overlap);
				off = next;
			}
		}
		return res;
	}

};



