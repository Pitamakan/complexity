#include "algorithms.h"
#include <vector>
#include <iostream>


int main()
{
	int n;
	while (std::cin >> n)
	{
		std::vector<std::string> strings(n);
		for (int i = 0; i < n; ++i)
		{
			std::cin >> strings[i];
		}
		std::cout << NPShortestSuperstring(strings).GetResult().size() << " ";
		std::cout << GreedyShortestSuperstring(strings).GetResult().size() << " ";
		std::cout << AssignShortestSuperstring(strings).GetResult().size() << std::endl;
	}
}